(define (S n) (+ n 1))

(define (recfun x n)
	(display (conc "\nWhatever: " n)) 
	(if (< n 100) (recfun x (+ n 1))))

(define (recfun2 x n upper)

	(display (conc "\nWhatever: " n))
	(if (< n upper) (recfun2 x (+ n 1) upper)
					(display "\n\nWe're done!\n\n")
	))

(define (recfun3 f x n upper)

	(display (conc "\nWhatever: " n))
	(if (< n upper) (recfun3 f x (f n 1) upper)
					(display "\n\nWe're done!\n\n")
	))

(define (recfun4 f p x n upper)

	(display (conc "\nWhatever: " n))
	(if (< n upper) (recfun4 f p x (f n p) upper)
					(display "\n\nWe're done!\n\n")
	)
)

(define (recfun5 advOp advPar initial upper)

	(display (conc "\nWhatever: " initial))
	(if (< initial upper) (recfun5 advOp advPar (advOp initial advPar) upper)
					(display "\n\nWe're done!\n\n")
	)
)

; Just renaming stuff...
(define (for initial advPar upper advOp)

	(display (conc "\nWhatever: " initial))
	(if (< initial upper) (for (advOp initial advPar) advPar upper advOp)
					(display "\n\nWe're done!\n\n")
	)
)

(define (for initial advPar upper advOp)

	(display (conc "\nWhatever: " initial))
	(if (< initial upper) (for (advOp initial advPar) advPar upper advOp)
					(display "\n\nWe're done!\n\n")
	)
)

(define (for initial compOp upper advOp advPar)
	(display (conc "\nWhatever: " initial))
	(if (compOp initial upper) (for (advOp initial advPar) compOp upper advOp advPar)
					(display "\n\nWe're done!\n\n")
	)
	
)

;for (int i = 0; i < upper; i++) {
;	// blah
;}


