(define (S n) (+ n 1))

(define (recfun x n)
	(display (conc "\nWhatever: " n)) 
	(if (< n 100) (recfun x (+ n 1))))

(define (recfun2 x n upper)

	(display (conc "\nWhatever: " n))
	(if (< n upper) (recfun2 x (+ n 1) upper)
					(display "\n\nWe're done!\n\n")
	))

(define (recfun3 f x n upper)

	(display (conc "\nWhatever: " n))
	(if (< n upper) (recfun3 f x (f n 1) upper)
					(display "\n\nWe're done!\n\n")
	))

(define (recfun4 f p x n upper)

	(display (conc "\nWhatever: " n))
	(if (< n upper) (recfun4 f p x (f n p) upper)
					(display "\n\nWe're done!\n\n")
	)
)

(define (recfun5 advOp advPar initial upper)

	(display (conc "\nWhatever: " initial))
	(if (< initial upper) (recfun5 advOp advPar (advOp initial advPar) upper)
					(display "\n\nWe're done!\n\n")
	)
)

; Just renaming stuff...
(define (for1 initial advPar upper advOp)

	(display (conc "\nWhatever: " initial))
	(if (< initial upper) (for1 (advOp initial advPar) advPar upper advOp)
					(display "\n\nWe're done!\n\n")
	)
)

(define (for2 initial advPar upper advOp)

	(display (conc "\nWhatever: " initial))
	(if (< initial upper) (for2 (advOp initial advPar) advPar upper advOp)
					(display "\n\nWe're done!\n\n")
	)
)

(define (for3 initial compOp upper advOp advPar body)
	;(display (conc "\nWhatever: " initial))
	(eval body (scheme-report-environment 5))
	(if (compOp initial upper) (for3 (advOp initial advPar) compOp upper advOp advPar body)
					(display "\n\nWe're done!\n\n")
	)
	;there is an off-by-one error, apparently; the loop goes 1 further step than it should.
	;Fix it!  RTL
)

;for (int i = 0; i < upper; i++) {
;	// blah
;}



(define (iDoSeveralThings)
           (display "Thing 1\n")
           (display "Thing 2\n")
           (display "Thing 3\n")

)

(define (ripOffThe6thC a b c d e f) f)

(define (ripOffThe6th l) (car (cdr (cdr (cdr (cdr (cdr l)))))))

(define (mandelbrot cre cim zre zim its retChar)
	   (let ((magn (+ (* zre zre) (* zim zim))))
	      (cond 
	       ((and (>= its 32) (>= magn 4.0)) '(cre cim zre zim its "."))
	       ((and (>= its 16) (>= magn 4.0)) '(cre cim zre zim its "o"))
	       ((>= magn 4.0) '(cre cim zre zim its "O"))
	       ((>= its 1000000) '(cre cim zre zim its " "))
	       (else (mandelbrot cre cim (+ (- (* zre zre) (* zim zim)) cre) (+ (* 2 (* zre zim)) cim) (+ its 1) "-")))
	            
	      ;(display magn)
	   )

	   ;(display retChar)
	  
)

(define (drawSquare)
  (for3 0 < 5 + 1 (for3 0 < 5 + 1 (display "X"))))

(define (dispAndInc i)
  (display i)
  (+ i 1))

(define (breakAndReturnSuccessor a)
  (display "\n")
  (+ a 1))

(define (breakAndReturnPredecessor a)
  (display "\n")
  (- a 1))

(define (printAndReturnSuccessor a b)
  (display a)
  (+ b 1))

(define (innerLoop y)
  (do ((x -39 (printAndReturnSuccessor (ripOffThe6th (mandelbrot (- (/ x 40.0) 0.5) (/ y 20.0) 0.0 0.0 1 "-")) x)))
      ((>= x 40)
       (breakAndReturnPredecessor y))))

(define (outerLoop)
  (do ((j 19 (innerLoop j)))
      ((<= j -20))))

(define (mandelbench?)
  (outerLoop))
